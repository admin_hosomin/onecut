//
//  User.h
//  OneCut
//
//  Created by 羽田　健太郎 on 2015/07/17.
//  Copyright (c) 2015年 羽田　健太郎. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Timeline;

@interface User : NSManagedObject

@property (nonatomic, retain) NSString * image_path;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSSet *timelines;
@end

@interface User (CoreDataGeneratedAccessors)

- (void)addTimelinesObject:(Timeline *)value;
- (void)removeTimelinesObject:(Timeline *)value;
- (void)addTimelines:(NSSet *)values;
- (void)removeTimelines:(NSSet *)values;

@end
