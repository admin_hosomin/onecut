//
//  Movie.h
//  OneCut
//
//  Created by 羽田　健太郎 on 2015/07/17.
//  Copyright (c) 2015年 羽田　健太郎. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Timeline;

@interface Movie : NSManagedObject

@property (nonatomic, retain) NSNumber * caption_end_sec;
@property (nonatomic, retain) NSNumber * caption_start_sec;
@property (nonatomic, retain) NSNumber * filter_id;
@property (nonatomic, retain) NSString * path;
@property (nonatomic, retain) NSDate * update_at;
@property (nonatomic, retain) Timeline *timeline;
@property (nonatomic, retain) NSNumber * duration;

- (NSString*)fullPath;

@end
