//
//  UserManager.swift
//  OneCut
//
//  Created by 羽田　健太郎 on 2015/07/17.
//  Copyright (c) 2015年 羽田　健太郎. All rights reserved.
//  ** これはCoreDataのユーザーマネージャーじゃなくてあれね、アプリをログインしているユーザー情報ね

import UIKit

class UserManager: NSObject {
    var loginUser:User!
    
    class var sharedInstance : UserManager {
        struct Static {
            static var onceToken : dispatch_once_t = 0
            static var instance : UserManager? = nil
        }
        dispatch_once(&Static.onceToken) {
            Static.instance = UserManager()
        }
        return Static.instance!
    }
    
    func postTimeline(title:String, desc:String, path:String, caption_start:Float!,caption_end:Float!){
        var timeline = Timeline.MR_createEntityInContext(self.loginUser.managedObjectContext!)!
        timeline.title = title
        timeline.desc = desc
        timeline.movie = self.createMovieToTimeLine(timeline, path: path, caption_start: caption_start, caption_end: caption_end)
        timeline.user = self.loginUser
        timeline.created_at = NSDate()
        self.loginUser.addTimelinesObject(timeline)
        self.loginUser.managedObjectContext?.MR_saveToPersistentStoreWithCompletion(
            { (result, err) -> Void in
                // Saved then show Caption View
                NSNotificationCenter.defaultCenter().postNotificationName(OCNotificationType.savedMovie.rawValue, object: nil, userInfo: [OCNotificationType.savedMovieKey.rawValue:timeline])
            }
        )
        
    }
    
    func createMovieToTimeLine(timeline:Timeline, path:String, caption_start:Float!,caption_end:Float!)->Movie{
        var movie = Movie.MR_createEntityInContext(timeline.managedObjectContext!)!
        movie.path = path.stringByReplacingOccurrencesOfString(NSBundle.mainBundle().resourcePath!, withString: "", options: [], range: nil)
        movie.caption_end_sec = caption_end
        movie.caption_start_sec = caption_start
        movie.update_at = NSDate()
        timeline.movie = movie
        return movie
    }
}
