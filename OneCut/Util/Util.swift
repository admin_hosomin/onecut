//
//  Util.swift
//  OneCut
//
//  Created by 羽田　健太郎 on 2015/07/12.
//  Copyright (c) 2015年 羽田　健太郎. All rights reserved.
//

import UIKit

class Util: NSObject {
    
    class func dateStr()->String{
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd-HH:mm:ss"
        return dateFormatter.stringFromDate(NSDate())
    }
    
}
