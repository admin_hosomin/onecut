//
//  Timeline.m
//  OneCut
//
//  Created by 羽田　健太郎 on 2015/07/17.
//  Copyright (c) 2015年 羽田　健太郎. All rights reserved.
//

#import "Timeline.h"
#import "Movie.h"
#import "User.h"


@implementation Timeline

@dynamic created_at;
@dynamic desc;
@dynamic title;
@dynamic movie;
@dynamic user;

@end
