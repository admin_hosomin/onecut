//
//  Recorder.swift
//  OneCut
//
//  Created by 羽田　健太郎 on 2015/07/12.
//  Copyright (c) 2015年 羽田　健太郎. All rights reserved.
//

import UIKit
import AVFoundation
import AssetsLibrary

class Recorder : NSObject{
    
    let captureSession = AVCaptureSession()
    let videoDevice = AVCaptureDevice.defaultDeviceWithMediaType(AVMediaTypeVideo)
    let audioDevice = AVCaptureDevice.defaultDeviceWithMediaType(AVMediaTypeAudio)
    var videoWriter : VideoWriter?
    
    var isCapturing = false
    var isPaused = false
    var isDiscontinue = false
    var fileIndex = 0
    
    var timeOffset = CMTimeMake(0, 0)
    var lastAudioPts: CMTime?
    
    let lockQueue = dispatch_queue_create("com.onecut.lockQueue", nil)
    let recordingQueue = dispatch_queue_create("com.onecut.RecordingQueue", DISPATCH_QUEUE_SERIAL)
    
    
    func shutdown(){
        self.captureSession.stopRunning()
    }
    
    func start(){
        //dispatch_sync(self.lockQueue) {
            if !self.isCapturing{
                Logger.log("in")
                self.isPaused = false
                self.isDiscontinue = false
                self.isCapturing = true
                self.timeOffset = CMTimeMake(0, 0)
            }
        //}
    }
    
    func stop(){
        //dispatch_sync(self.lockQueue) {
            if self.isCapturing{
                self.isCapturing = false
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    Logger.log("in")
                    self.videoWriter!.finish { () -> Void in
                        Logger.log("Recording finished.")
                        self.videoWriter = nil
                        let assetsLib = ALAssetsLibrary()
                        assetsLib.writeVideoAtPathToSavedPhotosAlbum(self.filePathUrl(), completionBlock: {
                            (nsurl, error) -> Void in
                            Logger.log("Transfer video to library finished.")
                            print(self.filePathUrl().absoluteString)
                            print(nsurl)
                            let paths = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)
                            let documentsDirectory = paths[0] 
                            let savePath = self.filePathUrl().absoluteString!.stringByReplacingOccurrencesOfString("file://"+documentsDirectory, withString: "", options: [], range: nil)
                            
                            UserManager.sharedInstance.postTimeline("TEST",
                                desc: "test",
                                path: savePath,
                                caption_start: nil, caption_end: nil)
                            self.fileIndex++
                        })
                    }
                })
            }
        //}
    }
    
    func pause(){
        dispatch_sync(self.lockQueue) {
            if self.isCapturing{
                Logger.log("in")
                self.isPaused = true
                self.isDiscontinue = true
            }
        }
    }
    
    func resume(){
        dispatch_sync(self.lockQueue) {
            if self.isCapturing{
                Logger.log("in")
                self.isPaused = false
            }
        }
    }
    
    func captureOutputFromGPUCamera(didOutputSampleBuffer sampleBuffer: CMSampleBuffer!, size:CGSize){
        dispatch_sync(self.lockQueue) {
            if !self.isCapturing || self.isPaused {
                return
            }
            
            let isVideo = true
            
            // Video writer setup
            if self.videoWriter == nil {
                let fileManager = NSFileManager()
                if fileManager.fileExistsAtPath(self.filePath()) {
                    do {
                        try fileManager.removeItemAtPath(self.filePath())
                    } catch _ {
                    }
                }
                
                let fmt = CMSampleBufferGetFormatDescription(sampleBuffer)
                let asbd = CMAudioFormatDescriptionGetStreamBasicDescription(fmt!)
                
                
                Logger.log("setup video writer")
                self.videoWriter = VideoWriter(
                    fileUrl: self.filePathUrl(),
                    height: Int(size.height), width: Int(size.width)
                )
            }
            
            // 撮影再開
            if self.isDiscontinue {
                
                var pts = CMSampleBufferGetPresentationTimeStamp(sampleBuffer)
                
                let isAudioPtsValid = self.lastAudioPts!.flags.intersect(CMTimeFlags.Valid)
                if isAudioPtsValid.rawValue != 0 {
                    Logger.log("isAudioPtsValid is valid")
                    let isTimeOffsetPtsValid = self.timeOffset.flags.intersect(CMTimeFlags.Valid)
                    if isTimeOffsetPtsValid.rawValue != 0 {
                        Logger.log("isTimeOffsetPtsValid is valid")
                        pts = CMTimeSubtract(pts, self.timeOffset);
                    }
                    let offset = CMTimeSubtract(pts, self.lastAudioPts!);
                    
                    if (self.timeOffset.value == 0)
                    {
                        Logger.log("timeOffset is \(self.timeOffset.value)")
                        self.timeOffset = offset;
                    }
                    else
                    {
                        Logger.log("timeOffset is \(self.timeOffset.value)")
                        self.timeOffset = CMTimeAdd(self.timeOffset, offset);
                    }
                }
                self.lastAudioPts!.flags = CMTimeFlags()
                self.isDiscontinue = false
            }
            
            // 録画
            var buffer = sampleBuffer
            if self.timeOffset.value > 0 {
                buffer = self.ajustTimeStamp(sampleBuffer, offset: self.timeOffset)
            }
            
            if self.videoWriter != nil {
                self.videoWriter?.write(buffer, isVideo: isVideo)
            }
        }
    }

    
    func filePath() -> String {
        let documentsDirectory = (NSHomeDirectory() as NSString).stringByAppendingPathComponent("Documents")
        let filePath : String = "\(documentsDirectory)/video\(self.fileIndex).mp4"
        let fileURL : NSURL = NSURL(fileURLWithPath: filePath)
        return filePath
    }
    
    func filePathUrl() -> NSURL! {
        return NSURL(fileURLWithPath: self.filePath())
    }
    
    func ajustTimeStamp(sample: CMSampleBufferRef, offset: CMTime) -> CMSampleBufferRef {
        var count: CMItemCount = 0
        CMSampleBufferGetSampleTimingInfoArray(sample, 0, nil, &count);
        var info = [CMSampleTimingInfo](count: count, repeatedValue: CMSampleTimingInfo(duration: CMTimeMake(0, 0), presentationTimeStamp: CMTimeMake(0, 0), decodeTimeStamp: CMTimeMake(0, 0)))
        CMSampleBufferGetSampleTimingInfoArray(sample, count, &info, &count);
        
        for i in 0..<count {
            info[i].decodeTimeStamp = CMTimeSubtract(info[i].decodeTimeStamp, offset);
            info[i].presentationTimeStamp = CMTimeSubtract(info[i].presentationTimeStamp, offset);
        }
        
        //var out: Unmanaged<CMSampleBuffer>?
        //CMSampleBufferCreateCopyWithNewTiming(nil, sample, count, &info, out);
        //var out: UnsafeMutablePointer<CMSampleBuffer?> = UnsafeMutablePointer.alloc(1)
        var out:CMSampleBuffer?
        CMSampleBufferCreateCopyWithNewTiming(nil, sample, count, &info, &out)
        return out!
    }
}
