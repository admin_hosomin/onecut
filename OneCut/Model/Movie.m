//
//  Movie.m
//  OneCut
//
//  Created by 羽田　健太郎 on 2015/07/17.
//  Copyright (c) 2015年 羽田　健太郎. All rights reserved.
//

#import "Movie.h"
#import "Timeline.h"


@implementation Movie

@dynamic caption_end_sec;
@dynamic caption_start_sec;
@dynamic filter_id;
@dynamic path;
@dynamic update_at;
@dynamic timeline;
@dynamic duration;

- (NSString*)fullPath{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *DocumentsDirPath = [paths objectAtIndex:0];
    return [NSString stringWithFormat:@"%@%@", DocumentsDirPath, self.path];
}
@end
