//
//  User.m
//  OneCut
//
//  Created by 羽田　健太郎 on 2015/07/17.
//  Copyright (c) 2015年 羽田　健太郎. All rights reserved.
//

#import "User.h"
#import "Timeline.h"


@implementation User

@dynamic image_path;
@dynamic name;
@dynamic timelines;

@end
