//
//  CaptureOverlayView.swift
//  OneCut
//
//  Created by 羽田　健太郎 on 2015/07/18.
//  Copyright (c) 2015年 羽田　健太郎. All rights reserved.
//

import UIKit

class CaptureOverlayView: UIView {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor.whiteColor()
        self.alpha = 0.7
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func hitTest(point: CGPoint, withEvent event: UIEvent!) -> UIView?
    {
        return nil
    }

}
