//
//  ViewController.swift
//  OneCut
//
//  Created by 羽田　健太郎 on 2015/06/28.
//  Copyright (c) 2015年 羽田　健太郎. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UIScrollViewDelegate, HomeTableViewCellDelegate {

    @IBOutlet weak var timelineTableView: UITableView!
    let homeTableViewHeight:CGFloat = 540.0
    var sectionHeaderHeight:CGFloat = 0
    var timelines = [Timeline]()

    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // セクションヘッダの高さ
        sectionHeaderHeight = self.view.frame.height / 568 * 320
        
        // indicator 非表示
        timelineTableView.showsVerticalScrollIndicator = false
        
        // separator 非表示
        timelineTableView.separatorColor = UIColor.clearColor()
        
        self.timelineTableView.registerClass(UITableViewCell.self, forCellReuseIdentifier: "topCell")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidAppear(animated: Bool) {
        self.timelines = Timeline.MR_findAllSortedBy("created_at", ascending: false) as! [Timeline]
        // Regist Cell
        var nib:UINib = UINib(nibName: "HomeTableViewCell", bundle: nil)
        for i in 1...10 {
            self.timelineTableView.registerNib(nib, forCellReuseIdentifier: "Cell\(i)")
        }
        self.timelineTableView.reloadData()
    }
    
    override func prefersStatusBarHidden() -> Bool {
        return true
    }
    
    // MARK: - TableView Delegate
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if (indexPath.row == 0) {
            return sectionHeaderHeight
        }
        return homeTableViewHeight
    }

    // セルの行数
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.timelines.count
    }
    
    // セルの内容を変更
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        // 最初の cell
        if (indexPath.row == 0) {
            let cell = tableView.dequeueReusableCellWithIdentifier("topCell", forIndexPath: indexPath) 
            let topImageView = UIImageView(frame: CGRectMake(0, 0, self.view.frame.width, sectionHeaderHeight))
            topImageView.image = UIImage(named: "timeline_image_top.png")
            cell.backgroundView = topImageView
            return cell
        }
        
        // 再利用するCellを取得する.
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell\(indexPath.row % 10 + 1)", forIndexPath: indexPath) as! HomeTableViewCell
        cell.index = indexPath.row-1
        cell.superView = self.view
        cell.timelineData = self.timelines[indexPath.row-1]
        cell.delegate = self
        
        // cell の両端の mergin をなくす
        cell.separatorInset = UIEdgeInsetsZero
        cell.preservesSuperviewLayoutMargins = false
        cell.layoutMargins = UIEdgeInsetsZero
        
        /*for subView in cell.contentView.subviews {
            if let subview = subView as? UIView {
                subView.removeFromSuperview()
            }
        }*/

        return cell
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        //return sectionHeaderHeight
        return 0
    }
    
    // MARK: - ScrollView Delegate
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
        let scrollOffsetY = scrollView.contentOffset.y
        // 通知を送る, Cellに通知を送る
        NSNotificationCenter.defaultCenter().postNotificationName(NotificationHomeTableViewCellUpdate, object: nil, userInfo: ["timelineContentOffsetY":scrollOffsetY])
    }
    
    // MARK: HomeTableViewCellDelegate
    func touchMoviePreviewImg(timeline: Timeline, fileUrl: NSURL) {
        var vc = AVPlayerViewController()
        let playerItem = AVPlayerItem(URL: fileUrl)
        let player = AVPlayer(playerItem: playerItem)
        vc.player = player
        self.presentViewController(vc, animated: true, completion: nil)
    }
}

