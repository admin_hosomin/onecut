//
//  Filters.swift
//  OneCut
//
//  Created by 羽田　健太郎 on 2015/07/11.
//  Copyright (c) 2015年 羽田　健太郎. All rights reserved.
//

import UIKit

class Filters: NSObject {
    
    // Filters
    var selectedFilter:GPUImageFilter!
    var lists:[GPUImageFilter?] = [GPUImageFilter?]()
    var selectedIndex:Int!{
        didSet{
            selectedFilter = self.lists[self.selectedIndex]
        }
    }
    
    override init() {
        lists.append(nil)
        lists.append(GPUImagePixellateFilter())
        lists.append(GPUImagePolkaDotFilter())
    }
    
    class var sharedInstance : Filters {
        struct Static {
            static var onceToken : dispatch_once_t = 0
            static var instance : Filters? = nil
        }
        dispatch_once(&Static.onceToken) {
            Static.instance = Filters()
        }
        return Static.instance!
    }
}
