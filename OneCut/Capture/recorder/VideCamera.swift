//
//  VideCamera.swift
//  OneCut
//
//  Created by 羽田　健太郎 on 2015/07/12.
//  Copyright (c) 2015年 羽田　健太郎. All rights reserved.
//

import UIKit

class VideCamera: NSObject,GPUImageVideoCameraDelegate {
    
    let recorder = Recorder()
    var videoCamera:GPUImageVideoCamera?
    var writer:GPUImageMovieWriter!
    var cameraSize:CGSize!
  
    func setup(screenView:GPUImageView){
        
        videoCamera = GPUImageVideoCamera(sessionPreset: AVCaptureSessionPresetHigh, cameraPosition: .Back)
        videoCamera?.delegate = self
        videoCamera?.outputImageOrientation = .Portrait;
        videoCamera?.startCameraCapture()
        videoCamera?.addTarget(screenView)
        
        let output = videoCamera!.captureSession.outputs[videoCamera!.captureSession.outputs.endIndex - 1] as! AVCaptureVideoDataOutput
        let setting:NSDictionary = output.videoSettings
        let width  = CGFloat(setting["Width"] as! NSNumber)
        let height = CGFloat(setting["Height"] as! NSNumber)
        self.cameraSize = CGSizeMake(width, height)
        
        writer = GPUImageMovieWriter(movieURL: recorder.filePathUrl(), size: self.cameraSize)
        
    }
    
    // MARK: GPUImageVideoCameraDelegate
    func willOutputSampleBuffer(sampleBuffer: CMSampleBuffer!) {
        if self.cameraSize != nil{
            recorder.captureOutputFromGPUCamera(didOutputSampleBuffer: sampleBuffer, size: self.cameraSize)
        }
    }
    
    // MARK: Control Camera
    func startCapture(){
        if !self.recorder.isCapturing {
            self.recorder.start()
            print("start")
        }
    }
    
    func stopCapture(){
        self.recorder.stop()
    }
    
    func setFilterWithSelectIndex(selectIndex:Int, captureImage:GPUImageView){
        if selectIndex == 0{
            self.videoCamera?.removeTarget(captureImage)
            self.videoCamera?.removeTarget(Filters.sharedInstance.selectedFilter)
            Filters.sharedInstance.selectedIndex = selectIndex
            videoCamera?.addTarget(captureImage)
            
        }else{
            self.videoCamera?.removeTarget(captureImage)
            self.videoCamera?.removeTarget(Filters.sharedInstance.selectedFilter)
            Filters.sharedInstance.selectedIndex = selectIndex
            self.videoCamera?.addTarget(Filters.sharedInstance.selectedFilter)
            Filters.sharedInstance.selectedFilter.addTarget(captureImage)
        }
    }
    
    
}
