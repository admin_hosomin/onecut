//
//  GPUCaptureViewController.swift
//  OneCut
//
//  Created by 羽田　健太郎 on 2015/07/11.
//  Copyright (c) 2015年 羽田　健太郎. All rights reserved.
//

import UIKit

class GPUCaptureViewController: UIViewController ,UICollectionViewDelegate,UICollectionViewDataSource,CaptureButtonDelegate{

    @IBOutlet weak var captureProgress: NSLayoutConstraint!
    @IBOutlet weak var captureImage: GPUImageView!
    @IBOutlet weak var captureBtn: CaptureButton!
    let videoCamera = VideCamera()
    var timeline:Timeline!

    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        videoCamera.setup(self.captureImage)
        self.captureBtn.delegate = self
        self.captureBtn.addTarget(self, action: "startCapture:", forControlEvents: .TouchDown)
        self.captureBtn.addTarget(self, action: "stopCapture:", forControlEvents: .TouchUpInside)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "savedMovieNotice:", name: OCNotificationType.savedMovie.rawValue, object: nil)
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        KSToastView.ks_showToast("3秒以上押すと録画されて\n勝手に画面遷移するよ！", duration: 1.5)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: Capture method
    func startCapture(sender: UIButton){
        self.videoCamera.startCapture()
    }
    
    func stopCapture(sender: UIButton){
        if self.captureBtn.isEnableToSave{
            self.videoCamera.stopCapture()
            SVProgressHUD.show()
            // 録画完了後、保存完了するとメソッドがよばれる -> savedMovieNotice
        }else{
            self.captureProgress.constant = 0
        }
    }
    
    
    // MARK: Call from Notification from UserManager
    func savedMovieNotice(notification: NSNotification?){
        if let userInfo = notification?.userInfo{
            if let timeline = userInfo[OCNotificationType.savedMovieKey.rawValue] as? Timeline{
                SVProgressHUD.dismiss()
                self.timeline = timeline
                self.performSegueWithIdentifier("push_caption",sender: nil)
            }
        }
    }

    // MARK: - Outlet
    @IBAction func close(){
        self.dismissViewControllerAnimated(true, completion: nil)
    }


    // MARK: - UICollectionViewDelegate Protocol
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell:UICollectionViewCell = (collectionView.dequeueReusableCellWithReuseIdentifier("cell", forIndexPath: indexPath) as? UICollectionViewCell)!
        cell.backgroundColor = UIColor.clearColor()
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        self.videoCamera.setFilterWithSelectIndex(indexPath.row, captureImage: self.captureImage)
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return Filters.sharedInstance.lists.count
    }
    
    // MARK: Segue delegate
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
        let vc:CaptionViewController = segue.destinationViewController as! CaptionViewController
        vc.timeline = self.timeline
    }
    
    // MARK: CaptureButtonDelegate
    func touchingTime(enableCaptureRate: Double) {
        let width = Double(self.view.frame.width) * enableCaptureRate
        captureProgress.constant = CGFloat(width)
        
        if enableCaptureRate > 0.99{
            KSToastView.ks_showToast("OK!!", duration: 1.5)
        }
    }

}
