//
//  HighlightManager.swift
//  Movieのcaption_timeからハイライト計算とかするクラス
//  OneCut
//
//  Created by 羽田　健太郎 on 2015/07/18.
//  Copyright (c) 2015年 羽田　健太郎. All rights reserved.
//
import UIKit

class HighlightManager: NSObject {
    
    /*
    再生されるコマ画像を作る
    これは均等に分けた時
    */
    class func calcHilightCationTimeNormalSerial(movie:Movie, captureCnt:Int, durationSecond:Float64)->[Float64]{
        var captureTimes = [Float64]()
        for(var i = 0; i < captureCnt; i++){
            var sec:Float64 = Float64(durationSecond)/Float64(captureCnt)*Float64(i)
            captureTimes.append(sec)
//            var secCMTime:CMTime = CMTimeMake(Int64(sec * 600), 600)
//            captureTImes[i] = NSValue(CMTime:secCMTime)
        }
        return captureTimes
    }
    
    /*
    再生されるコマ画像を作る
    ハイライト部分の割合を調整してみる
    */
    class func calcHilightCationTimeFocusSerial(movie:Movie, captureCnt:Int, durationSecond:Float64)->[Float64]{
        var captureTimes = [Float64]()
        
        // なんかいいロジックが思いつかない
        // とりあえず以下の数値が全体に占めるハイライトの割合になるようにする
        // hitakemu: 0.625 * (focus 部分の動画に占める割合) + 0.375 とかにひとまずしてみた
        // hitakemu: 20% の部分が選択されている時に 50% に拡大，100% 選択時はそのままになるような式にしてみた
        //movie.caption_start_sec = durationSecond * 0.2  // ひとまず
        //movie.caption_end_sec = durationSecond * 0.4  // ひとまず
        
        if movie.caption_end_sec == nil && movie.caption_start_sec == nil{
            return self.calcHilightCationTimeNormalSerial(movie, captureCnt: captureCnt, durationSecond: durationSecond)
        }
        
        
        let focusDuration = movie.caption_end_sec.doubleValue - movie.caption_start_sec.doubleValue
        let focusRate = 0.625 * focusDuration / durationSecond + 0.375
        let splitFocusCnt = min(Int(Double(captureCnt)*focusRate), 100)
        print(splitFocusCnt)

        // focusの部分
        for(var i = 0; i < splitFocusCnt; i++){
            var sec:Float64 = Float64(focusDuration)/Float64(splitFocusCnt)*Float64(i) + movie.caption_start_sec.doubleValue
            captureTimes.append(sec)
        }

        // それ以外の前後の時間
        let notFocusDuration = durationSecond - focusDuration
        let splitBeforeFocusCnt = Int(movie.caption_start_sec.doubleValue / notFocusDuration * Double(captureCnt - splitFocusCnt)) // 動画開始 ~ ハイライト開始
        let splitAfterFocusCnt = captureCnt - splitFocusCnt - splitBeforeFocusCnt // ハイライト終了 ~ 動画終了
        for(var i = 0; i < splitBeforeFocusCnt; i++){
            var sec:Float64 = Float64(movie.caption_start_sec.floatValue)/Float64(splitBeforeFocusCnt)*Float64(i)
            captureTimes.append(sec)
        }
        for(var i = 0; i < splitAfterFocusCnt; i++){
            var sec:Float64 = Float64(Float(durationSecond) - movie.caption_end_sec.floatValue)/Float64(splitAfterFocusCnt)*Float64(i)
            captureTimes.append(sec)
        }
        
        captureTimes.sortInPlace{ $0 < $1 }
        
        return Array(captureTimes[0...99])
    }
    
}
