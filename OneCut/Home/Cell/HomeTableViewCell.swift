//
//  HomeTableViewCell.swift
//  OneCut
//
//  Created by 羽田　健太郎 on 2015/06/28.
//  Copyright (c) 2015年 羽田　健太郎. All rights reserved.
//

import UIKit
import AVFoundation
import AssetsLibrary

protocol HomeTableViewCellDelegate {
    func touchMoviePreviewImg(timeline:Timeline, fileUrl:NSURL)
}

let NotificationHomeTableViewCellUpdate = "HomeTableViewCell"
let captureSuplitCnt:Int = 100

class HomeTableViewCell: UITableViewCell {

    @IBOutlet weak var moviePreviewImg: UIImageView!
    @IBOutlet weak var descriptionLbl: UILabel!
    @IBOutlet weak var userImg: UIImageView!
    @IBOutlet weak var nameLbl: UILabel!
    
    @IBOutlet weak var titleLbl: UILabel!
    var index:Int! = nil
    var superView:UIView!
    var captureImages = [UIImage?](count: captureSuplitCnt, repeatedValue: nil)
    var captureTimeIndex:[String:Int]! = [String:Int]()
    var filePath:String!
    var fileURL:NSURL!
    var delegate :HomeTableViewCellDelegate?
    
    var movie:AVURLAsset!
    var timelineData:Timeline! {
        didSet{
            self.titleLbl.text = timelineData.title
            self.nameLbl.text = timelineData.user.name
            if self.filePath != timelineData.movie.path{
                // initial
                self.filePath = timelineData.movie.path
                self.moviePreviewImg.image = UIImage(named: "loading.png")
                autoreleasepool {
                    captureImages = [UIImage?](count: captureSuplitCnt, repeatedValue: nil)
                }
                captureTimeIndex = [String:Int]()
                
                // test_は最初に入る動画
                if timelineData.movie.path.hasPrefix("/test_"){
                    self.fileURL = NSURL(fileURLWithPath: NSBundle.mainBundle().resourcePath!+timelineData.movie.path)
                    
                }else{
                    // 参照先はDocuments内のpath
                    self.fileURL = NSURL(fileURLWithPath: (NSHomeDirectory() as NSString).stringByAppendingPathComponent("Documents")+timelineData.movie.path)
                }
                print(fileURL)
                self.movie = AVURLAsset(URL: self.fileURL, options: nil)
                self.userImg.image = UIImage(contentsOfFile: NSBundle.mainBundle().resourcePath!+self.timelineData.user.image_path)
                self.generateCaptureImages()
            }
        }
    }
    
    // MARK: Default initial method

    override func awakeFromNib() {
        super.awakeFromNib()
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "controlPreviewImageWithTimelinePosition:", name: NotificationHomeTableViewCellUpdate, object: nil)
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    // MARK: touch delegate
    override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
        let touch :UITouch = (touches.first)!
        delegate?.touchMoviePreviewImg(self.timelineData, fileUrl: self.fileURL)
    }
    
    // MARK: image control method
    
    // scroll時に呼ばれる
    func controlPreviewImageWithTimelinePosition(notification: NSNotification?){
        if let userInfo = notification?.userInfo
        {
            if let conentOffsetY = userInfo["timelineContentOffsetY"] as? CGFloat
            {
                let previewHeadPos:CGPoint = CGPoint(x: self.moviePreviewImg.frame.origin.x, y: self.moviePreviewImg.frame.origin.y)
                // 画像のHome画面に対するviewからの相対位置
                let point:CGPoint = self.superView.convertPoint(previewHeadPos, fromView: self)
                if let showRate = self.calcShowRate(point){
                    let imgIndex = Int(showRate * CGFloat(self.captureImages.count))
                    if let img = self.captureImages[min(imgIndex+1, 99)]{
                        self.moviePreviewImg.image = img
                    }
                }
            }
        }
    }
    
    // 0.0 ~ 1.0
    func calcShowRate(imgShowPosition:CGPoint)->CGFloat!{
        var rate:CGFloat = 0.0
        var isShowImage:Bool = false
        if imgShowPosition.y > -self.moviePreviewImg.frame.height && imgShowPosition.y < UIScreen.mainScreen().bounds.height
        {
            if imgShowPosition.y < -self.moviePreviewImg.frame.height / 2 {
                return 1.0
            } else if imgShowPosition.y > UIScreen.mainScreen().bounds.height - self.moviePreviewImg.frame.height / 2 {
                return 0.0
            } else {
                let posRate:CGFloat = (imgShowPosition.y + CGFloat(self.moviePreviewImg.frame.height / 2)) / UIScreen.mainScreen().bounds.height
                return 1 - posRate
            }
        }
        return nil
    }
    
    func generateCaptureImages(){
        // パスからassetを生成.
        let avAsset = self.movie
        
        // assetから画像をキャプチャーする為のジュネレーターを生成.
        let generator = AVAssetImageGenerator(asset: avAsset)
        generator.requestedTimeToleranceAfter = kCMTimeZero
        generator.requestedTimeToleranceBefore = kCMTimeZero
        generator.maximumSize = self.moviePreviewImg.frame.size
        
        // 表示のタイムラグを避けるために 0 秒のキャプチャだけ別生成する
        var error : NSError?
        var actualTime : CMTime = CMTimeMake(0, 0)
        do{
        try self.moviePreviewImg.image = UIImage(CGImage: generator.copyCGImageAtTime(CMTimeMakeWithSeconds(0.0, 1), actualTime: &actualTime))
        }catch{
        }
        // 映像の長さからキャプチャ時間配列を生成
        var movieDuration = CMTimeGetSeconds(avAsset.duration)
        var captureTimeSecs = HighlightManager.calcHilightCationTimeFocusSerial(self.timelineData.movie, captureCnt: captureSuplitCnt, durationSecond:movieDuration)
        var captureTimeCMTimes = [NSValue](count: self.captureImages.count, repeatedValue: NSValue())
        for (var i = 0; i < captureTimeSecs.count; i++){
            self.captureTimeIndex[String(format:"%.2f", captureTimeSecs[i])] = i
                var secCMTime:CMTime = CMTimeMake(Int64(captureTimeSecs[i] * 600), 600)
                captureTimeCMTimes[i] = NSValue(CMTime:secCMTime)
        }
        
        // generate capture
        generator.generateCGImagesAsynchronouslyForTimes(captureTimeCMTimes, completionHandler: {
            (time:CMTime, img:CGImage?, cmttime:CMTime, result:AVAssetImageGeneratorResult,
            err:NSError?) -> Void in
            
            if let setImage = img{
                var sec = Float(CMTimeGetSeconds(time))
                if let timeIndex = self.captureTimeIndex[String(format:"%.2f", sec)]{

                    if (self.filePath.hasPrefix("/test_")){
                        self.captureImages[timeIndex] = UIImage(CGImage: setImage, scale: 1.0, orientation: UIImageOrientation.Up)
                    }else{
                        // 撮影された動画はなぜか左に90度回転しているのでまわす
                        self.captureImages[timeIndex] = UIImage(CGImage: setImage, scale: 1.0, orientation: UIImageOrientation.Right)
                    }
                    
                    // 最初の画像をset
                    if sec == 0{
                        self.moviePreviewImg.image = self.captureImages[timeIndex]
                    }
                }
            }
            
        })
    }
    
}
