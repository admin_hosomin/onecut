//
//  ShareViewController.swift
//  OneCut
//
//  Created by 羽田　健太郎 on 2015/07/17.
//  Copyright (c) 2015年 羽田　健太郎. All rights reserved.
//

import UIKit

class ShareViewController: UIViewController,UITextViewDelegate {

    @IBOutlet weak var moviePlayerView: MoviePlayerView!
    @IBOutlet weak var titleTextField: UIPlaceHolderTextView!
    var timeline:Timeline!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.moviePlayerView.timeline = timeline
        titleTextField.placeHolder = "この動画について何か書く"
        titleTextField.placeHolderColor = UIColor.lightGrayColor()
        titleTextField.delegate = self
        self.titleTextField.returnKeyType = .Done
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func share(sender: AnyObject) {
        timeline.title = titleTextField.text
        if (titleTextField.text.characters.count == 0){
            timeline.title = "No title."
        }
        timeline.managedObjectContext?.MR_saveToPersistentStoreWithCompletion({ (res, err) -> Void in
            self.dismissViewControllerAnimated(true, completion: nil)
        })
    }
    
    func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
        if (text == "\n"){
            self.titleTextField.resignFirstResponder()
            return false
        }
        return true
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
