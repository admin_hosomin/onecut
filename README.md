# Onecut

## ブランチ
devが基本的な開発ブランチ
ここからブランチを派生させて実装、完了後にマージする

## 実装メモ

### 20150718_khaneda
 - キャプションする画面 
  灰色の部分をなぞるとその時間帯をよしなに切り取ってリプレイし続ける＋DBに保存する
 - 撮影機能を実装
  フィルターは一応選択するとかかったように見えるけど実際には録画した映像には入らないｗ
 - 動画再生機能
 - モックデータを管理するマネージャーの実装
    ```
    // MockDataManaber
    // 注意点はこのモックデータと実際の構造はちょっとちがうってこと！！
    var mock_data =
    [
        ["name":"pikashi",// このユーザーの
            "image_path":NSBundle.mainBundle().pathForResource("pika_shi", ofType: "jpg")!,
            "timeline":
            [//タイムラインはこれで
                ["title":"華麗なるウェディングキス",
                    "desc":"",
                    "path":NSBundle.mainBundle().pathForResource("test_kiss", ofType: "mov")!,
                    "duration":14,//動画情報も入れておく
                    "caption_start":2,
                    "caption_end":6,
                ]
            ]
        ],

    ```
 - アプリ内DBの構造を変更
  User/Timeline/Movieを準備、それぞれRelationを持っていて、
  各ユーザーのタイムライン情報とそのタイムラインごとの動画が関連づいている状態になっている。
 
