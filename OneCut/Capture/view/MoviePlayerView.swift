//
//  MoviePlayerView.swift
//  OneCut
//
//  Created by 羽田　健太郎 on 2015/07/25.
//  Copyright (c) 2015年 羽田　健太郎. All rights reserved.
//

import UIKit

class MoviePlayerView: UIView ,GPUImageMovieDelegate{

    var playerView: GPUImageView!
    var player:AVPlayer!
    var gpuImageMovie:GPUImageMovie!
    var timeline:Timeline! {
        didSet{
            self.setUpViews()
        }
    }
    
    var startTime = NSTimeInterval()
    var timer = NSTimer()    
    
    // MARK:Initial methods
    func setUpViews(){
        timer = NSTimer.scheduledTimerWithTimeInterval(0.01, target: self, selector:Selector("onUpdate:"), userInfo: nil, repeats: true)

        var url:NSURL = NSURL(fileURLWithPath: self.timeline.movie.fullPath())
        
        self.playerView = GPUImageView(frame: CGRectMake(0, 0, self.frame.width, self.frame.height))
        self.addSubview(self.playerView)
        
        let item = AVPlayerItem(URL: url)
        player = AVPlayer(playerItem: item)
        player.play()
        
        gpuImageMovie = GPUImageMovie(playerItem: item)
        gpuImageMovie.playAtActualSpeed = true
        gpuImageMovie.delegate = self
        gpuImageMovie.shouldRepeat = true
        
        // なぜか左に90度回っているので直す
        var filter = GPUImageFilter()
        filter.addTarget(self.playerView)
        filter.setInputRotation(kGPUImageRotateRight, atIndex: 0)
        
        gpuImageMovie.addTarget(filter)
        gpuImageMovie.startProcessing()
    }
    
    func startFromSelectedPos(){
        self.player.play()
        self.player.seekToTime(CMTimeMakeWithSeconds(self.timeline.movie.caption_start_sec.doubleValue , 1))
        startTime = NSDate.timeIntervalSinceReferenceDate()
    }
    
    func onUpdate(timer : NSTimer){
        if (self.timeline.movie.caption_end_sec != nil){
            var currentTime = NSDate.timeIntervalSinceReferenceDate()
            let duration = currentTime - startTime
            if (self.timeline.movie.caption_start_sec.doubleValue + duration > self.timeline.movie.caption_end_sec.doubleValue){
                // Repeat処理
                self.startFromSelectedPos()
            }
        }
    }
    
    // MARK:GPUImageMovie - Delegate
    func didCompletePlayingMovie() {
    }
    
}
