//
//  CaptureButton.swift
//  VineVideo
//
//  Created by 羽田　健太郎 on 2015/07/11.
//  Copyright (c) 2015年 Takeshi Fujiki. All rights reserved.
//

import UIKit

protocol CaptureButtonDelegate {
    func touchingTime(enableCaptureRate:Double)
}

class CaptureButton: UIButton {

    var duration:NSTimeInterval = 0.0 {
        didSet{
            self.isEnableToSave = duration > captureNeedTime
        }
    }
    let captureNeedTime = 2.0
    var isEnableToSave = false
    var startTime = NSTimeInterval()
    var timer = NSTimer()
    var delegate:CaptureButtonDelegate?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.layer.masksToBounds = true
        self.layer.cornerRadius = self.bounds.width/2
        self.layer.borderColor = UIColor.whiteColor().CGColor
        self.layer.borderWidth = 5
        
        self.addTarget(self, action: "startCapture:", forControlEvents: .TouchDown)
        self.addTarget(self, action: "stopCapture:", forControlEvents: .TouchUpInside)
    }
    
    func startCapture(sender: UIButton){
        let aSelector : Selector = "updateTime"
        timer = NSTimer.scheduledTimerWithTimeInterval(0.01, target: self, selector: aSelector, userInfo: nil, repeats: true)
        startTime = NSDate.timeIntervalSinceReferenceDate()
    }
    
    func stopCapture(sender: UIButton){
        timer.invalidate()
    }
    
    func updateTime() {
        
        let currentTime = NSDate.timeIntervalSinceReferenceDate()
        self.duration = currentTime - startTime
        delegate?.touchingTime(self.duration/captureNeedTime)
    }
}
