//
//  CaptionViewController.swift
//  OneCut
//
//  Created by 羽田　健太郎 on 2015/07/17.
//  Copyright (c) 2015年 羽田　健太郎. All rights reserved.
//

import UIKit
import AVFoundation
import AssetsLibrary

let captureSelectSplitCtn:Int = 10

class CaptionViewController: UIViewController ,GPUImageMovieDelegate{

    @IBOutlet weak var playerView: GPUImageView!
    var player:AVPlayer!
    var gpuImageMovie:GPUImageMovie!
    var timeline:Timeline!{
        didSet{
        }
    }
    var captureStartPt:CGPoint!
    var captureEndPt:CGPoint!
    var captureOverLayView:CaptureOverlayView!
    var startDuration:Float64!
    var endDuration:Float64!
    
    var captureImages = [UIImage?](count: captureSelectSplitCtn, repeatedValue: nil)
    var captureTimeIndex:[String:Int]! = [String:Int]()

    
    var startTime = NSTimeInterval()
    var timer = NSTimer()
    
    @IBOutlet weak var selectCaptionView: UIView!

    // MARK:Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        self.setUpViews()
        timer = NSTimer.scheduledTimerWithTimeInterval(0.01, target: self, selector:Selector("onUpdate:"), userInfo: nil, repeats: true)
        KSToastView.ks_showToast("灰色のところをなぞってね！\n^q^", duration: 2.0)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK:Initial methods
    func setUpViews(){
        var url:NSURL = NSURL(fileURLWithPath: self.timeline.movie.fullPath())
        let item = AVPlayerItem(URL: url)
        player = AVPlayer(playerItem: item)
        player.play()
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "playerDidFinishPlaying:", name: AVPlayerItemDidPlayToEndTimeNotification, object: item)

        
        gpuImageMovie = GPUImageMovie(playerItem: item)
        gpuImageMovie.playAtActualSpeed = true
        gpuImageMovie.delegate = self
        gpuImageMovie.shouldRepeat = true
        
        // なぜか左に90度回っているので直す
        var filter = GPUImageFilter()
        filter.addTarget(self.playerView)
        filter.setInputRotation(kGPUImageRotateRight, atIndex: 0)
        
        gpuImageMovie.addTarget(filter)
        gpuImageMovie.startProcessing()
        
        self.setCaptionSelectImages()

    }
    
    // MARK; Player control
    func playerDidFinishPlaying(note: NSNotification) {
        self.player.play()
        self.player.seekToTime(CMTimeMakeWithSeconds(0 , 1))
    }
    
    // MARK:GPUImageMovie - Delegate
    func didCompletePlayingMovie() {
    }
    
    // MARK: Touch delegate
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        let touch :UITouch = (touches.first)!
        if touch.view == selectCaptionView{
            self.captureStartPt = touch.locationInView(touch.view)
        }
    }
    
    override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
        let touch :UITouch = (touches.first)!
        if touch.view == selectCaptionView{
            self.captureEndPt = touch.locationInView(touch.view)
            self.captured()
        }
    }
    
    // MARK: methods
    func captured(){
        if self.captureStartPt.x - self.captureEndPt.x >= 0{
            return
        }
        self.addCaptureOverlay()
        self.saveCaptionTime()
    }
    
    func addCaptureOverlay(){
        if self.captureStartPt != nil && self.captureEndPt != nil{
            let frame:CGRect = CGRectMake(self.captureStartPt.x, 0,
                self.captureEndPt.x - self.captureStartPt.x, self.selectCaptionView.frame.height)
            if self.captureOverLayView == nil
            {
                self.captureOverLayView = CaptureOverlayView(frame: frame)
                self.selectCaptionView.addSubview(self.captureOverLayView)
            } else {
                self.captureOverLayView.frame = frame
            }
        }
    }
    
    func saveCaptionTime(){
        let startRate = self.captureStartPt.x/self.selectCaptionView.frame.width
        let endRate = self.captureEndPt.x/self.selectCaptionView.frame.width
        
        self.startDuration = CMTimeGetSeconds(player.currentItem!.duration) * Float64(startRate)
        self.endDuration = CMTimeGetSeconds(player.currentItem!.duration) * Float64(endRate)
        
        print("----------SAVE CAPTION-------")
        print(startDuration)
        print(endDuration)
        print(player.currentItem!.duration.timescale)
        
        self.startFromSelectedPos()
        
        self.timeline.movie.caption_start_sec = NSNumber(double: startDuration)
        self.timeline.movie.caption_end_sec = NSNumber(double: startDuration)
        self.timeline.managedObjectContext?.MR_saveToPersistentStoreAndWait()
    }
    
    func setCaptionSelectImages(){
        // パスからassetを生成.
        let avAsset = AVURLAsset(URL: NSURL(fileURLWithPath: self.timeline.movie.fullPath()), options: nil)
        
        // assetから画像をキャプチャーする為のジュネレーターを生成.
        let generator = AVAssetImageGenerator(asset: avAsset)
        generator.requestedTimeToleranceAfter = kCMTimeZero
        generator.requestedTimeToleranceBefore = kCMTimeZero
        generator.maximumSize = CGSizeMake(60, 60)
        
        // 映像の長さからキャプチャ時間配列を生成
        var movieDuration = CMTimeGetSeconds(avAsset.duration)
        var captureTimeSecs = HighlightManager.calcHilightCationTimeNormalSerial(self.timeline.movie, captureCnt: captureSelectSplitCtn, durationSecond:movieDuration)
        var captureTimeCMTimes = [NSValue](count: self.captureImages.count, repeatedValue: NSValue())
        for (var i = 0; i < captureTimeSecs.count; i++){
            self.captureTimeIndex[String(format:"%.2f", captureTimeSecs[i])] = i
            var secCMTime:CMTime = CMTimeMake(Int64(captureTimeSecs[i] * 600), 600)
            captureTimeCMTimes[i] = NSValue(CMTime:secCMTime)
        }
        
        
        // generate capture
        generator.generateCGImagesAsynchronouslyForTimes(captureTimeCMTimes){
            (time:CMTime, img:CGImage?, cmttime:CMTime, result:AVAssetImageGeneratorResult,
            err:NSError?) -> Void in
            
            if let setImage = img{
                var sec = Float(CMTimeGetSeconds(time))
                if let timeIndex = self.captureTimeIndex[String(format:"%.2f", sec)]{
                    
                    var addImg:UIImage!
                    if (self.self.timeline.movie.path.hasPrefix("/test_")){
                        addImg = UIImage(CGImage: setImage, scale: 1.0, orientation: UIImageOrientation.Up)                    }else{
                        // 撮影された動画はなぜか左に90度回転しているのでまわす
                        addImg = UIImage(CGImage: setImage, scale: 1.0, orientation: UIImageOrientation.Right)
                    }
                    self.captureImages[timeIndex] = addImg
                    self.addCaptionImageToSelectView(addImg, time: sec, movieDuration:movieDuration)
                }
            }
        }
    }
    
    func dispatch_async_main(block: () -> ()) {
        dispatch_async(dispatch_get_main_queue(), block)
    }
    
    func dispatch_async_global(block: () -> ()) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), block)
    }
    
    func addCaptionImageToSelectView(img:UIImage, time:Float, movieDuration:Float64){
        
        self.dispatch_async_main { // ここからメインスレッド
            let rate = time / Float(movieDuration)
            let setPosX:CGFloat = CGFloat(rate * Float(self.selectCaptionView.frame.width))
            let v:UIImageView = UIImageView(image: img)
            v.frame = CGRectMake(setPosX, 0, 60, self.selectCaptionView.frame.height)
            self.selectCaptionView.addSubview(v)
        }
    }
    
    func startFromSelectedPos(){
        self.player.play()
        self.player.seekToTime(CMTimeMakeWithSeconds(startDuration , 1))
        startTime = NSDate.timeIntervalSinceReferenceDate()
    }
    
    func onUpdate(timer : NSTimer){
        if (self.endDuration != nil){
            let currentTime = NSDate.timeIntervalSinceReferenceDate()
            let duration = currentTime - startTime
            if (self.startDuration + duration > self.endDuration){
                // Repeat処理
                self.startFromSelectedPos()
            }
        }
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "share_push"{
            let vc = segue.destinationViewController as! ShareViewController
            vc.timeline = self.timeline
        }
    }
    

}
