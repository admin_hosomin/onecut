//
//  TimelineManager.swift
//  OneCut
//
//  Created by 羽田　健太郎 on 2015/07/12.
//  Copyright (c) 2015年 羽田　健太郎. All rights reserved.
//

import UIKit

class TimelineManager: NSObject {
    
    class func save(fileName:String){
        
        var filePath = "/"+fileName+".MOV"

        let tm = Timeline.MR_createEntity()!
        tm.title = fileName+" title"
        tm.desc = fileName
        tm.created_at = NSDate()
        tm.managedObjectContext?.MR_saveToPersistentStoreAndWait()
    }
}
