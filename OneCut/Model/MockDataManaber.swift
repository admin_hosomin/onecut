//
//  MockDataManaber.swift
//  OneCut
//
//  Created by 羽田　健太郎 on 2015/07/16.
//  Copyright (c) 2015年 羽田　健太郎. All rights reserved.
//

import UIKit

class MockDataManaber: NSObject {
    
    let pikashi_data =
    ["name":"pikashi",
        "image_path":NSBundle.mainBundle().pathForResource("pika_shi", ofType: "jpg")!,
        "timeline":
        [
            ["title":"華麗なるウェディングキス",
             "desc":"",
             "path":NSBundle.mainBundle().pathForResource("test_kiss", ofType: "mov")!,
             "duration":14,
             "caption_start":2,
             "caption_end":6,
            ],
            ["title":"これすげー！笑",
                "desc":"",
                "path":NSBundle.mainBundle().pathForResource("test_funnystudent", ofType: "mov")!,
                "duration":6,
                "caption_start":0,
                "caption_end":6,
            ],
            ["title":"甥っ子かわいすぎる…",
                "desc":"",
                "path":NSBundle.mainBundle().pathForResource("test_baby2", ofType: "mov")!,
                "duration":16,
                "caption_start":8,
                "caption_end":10,
            ],
            ["title":"ミッキーとミニー仲良し♥",
                "desc":"",
                "path":NSBundle.mainBundle().pathForResource("test_disney", ofType: "mov")!,
                "duration":14,
                "caption_start":5,
                "caption_end":8,
            ],
            ["title":"優子ちゃんおめでとう！",
                "desc":"",
                "path":NSBundle.mainBundle().pathForResource("test_marry", ofType: "mov")!,
                "duration":6,
                "caption_start":0,
                "caption_end":6,
            ]
        ]
    ]

    let jumbo_data =
    ["name":"jumbOS5",
        "image_path":NSBundle.mainBundle().pathForResource("jumbOS5", ofType: "jpg")!,
        "timeline":
            [
                ["title":"イヌローさん胴上げわず",
                    "desc":"",
                    "path":NSBundle.mainBundle().pathForResource("test_inuro", ofType: "mov")!,
                    "duration":8,
                    "caption_start":3,
                    "caption_end":6,
                ],
                ["title":"タマにドラマーやらせてみた",
                    "desc":"",
                    "path":NSBundle.mainBundle().pathForResource("test_cat", ofType: "mov")!,
                    "duration":22,
                    "caption_start":0,
                    "caption_end":22,
                ],
                ["title":"Hevesh5!",
                    "desc":"",
                    "path":NSBundle.mainBundle().pathForResource("test_domino", ofType: "mov")!,
                    "duration":5,
                    "caption_start":0,
                    "caption_end":5,
                ],
                ["title":"巻き込み事故w",
                    "desc":"",
                    "path":NSBundle.mainBundle().pathForResource("test_hamustar", ofType: "mov")!,
                    "duration":25,
                    "caption_start":15,
                    "caption_end":25,
                ],
                ["title":"マミちゃんーー！",
                    "desc":"",
                    "path":NSBundle.mainBundle().pathForResource("test_mami", ofType: "mov")!,
                    "duration":8,
                    "caption_start":6,
                    "caption_end":8,
                ],
                ["title":"狙い撃ち☆",
                    "desc":"",
                    "path":NSBundle.mainBundle().pathForResource("test_momokuro", ofType: "mov")!,
                    "duration":6,
                    "caption_start":3,
                    "caption_end":5,
                ],
                ["title":"ブーケトス、花子ちゃんがとった！",
                    "desc":"",
                    "path":NSBundle.mainBundle().pathForResource("test_toss", ofType: "mov")!,
                    "duration":11,
                    "caption_start":3,
                    "caption_end":6,
                ]
        ]
    ]

    let shoshomi_data =
    ["name":"shosomi",
        "image_path":NSBundle.mainBundle().pathForResource("shosomi", ofType: "jpg")!,
        "timeline":
            [
                ["title":"角度詐欺やってみた(^O^)／",
                    "desc":"",
                    "path":NSBundle.mainBundle().pathForResource("test_fraud", ofType: "mov")!,
                    "duration":7,
                    "caption_start":4,
                    "caption_end":6,
                ],
                ["title":"うまとび",
                    "desc":"",
                    "path":NSBundle.mainBundle().pathForResource("test_umatobi", ofType: "mov")!,
                    "duration":6,
                    "caption_start":2,
                    "caption_end":3,
                ],
                ["title":"ちょっと描いてみた",
                    "desc":"",
                    "path":NSBundle.mainBundle().pathForResource("test_illust", ofType: "mov")!,
                    "duration":28,
                    "caption_start":0,
                    "caption_end":25,
                ],
                ["title":"開けたらびっくり！",
                    "desc":"",
                    "path":NSBundle.mainBundle().pathForResource("test_nabe", ofType: "mov")!,
                    "duration":2,
                    "caption_start":0,
                    "caption_end":2,
                ],
                ["title":"すげー！ひやひやしたw",
                    "desc":"",
                    "path":NSBundle.mainBundle().pathForResource("test_performer", ofType: "mov")!,
                    "duration":7,
                    "caption_start":0,
                    "caption_end":7,
                ],
                ["title":"レインボー♥",
                    "desc":"",
                    "path":NSBundle.mainBundle().pathForResource("test_rainbow", ofType: "mov")!,
                    "duration":22,
                    "caption_start":0,
                    "caption_end":22,
                ],
                ["title":"スタバで休憩☆",
                    "desc":"",
                    "path":NSBundle.mainBundle().pathForResource("test_starbacks", ofType: "mov")!,
                    "duration":15,
                    "caption_start":4,
                    "caption_end":7,
                ]
        ]
    ]
    
    let hajipion_data =
    ["name":"hajipion",
        "image_path":NSBundle.mainBundle().pathForResource("hajipion", ofType: "jpg")!,
        "timeline":
            [
                ["title":"隅田川の花火大会なう♡",
                    "desc":"",
                    "path":NSBundle.mainBundle().pathForResource("test_fireworks", ofType: "mov")!,
                    "duration":24,
                    "caption_start":10,
                    "caption_end":15,
                ],
                ["title":"ウユニなう",
                    "desc":"",
                    "path":NSBundle.mainBundle().pathForResource("test_uyuni", ofType: "mov")!,
                    "duration":12,
                    "caption_start":6,
                    "caption_end":10,
                ],
                ["title":"今回のキルラキルも熱かったな〜",
                    "desc":"",
                    "path":NSBundle.mainBundle().pathForResource("test_killlakill", ofType: "mov")!,
                    "duration":20,
                    "caption_start":0,
                    "caption_end":20,
                ],
                ["title":"ウユニ最高！！",
                    "desc":"",
                    "path":NSBundle.mainBundle().pathForResource("test_uyuni2", ofType: "mov")!,
                    "duration":10,
                    "caption_start":1,
                    "caption_end":5,
                ],
                ["title":"サンドアートフェスティバルきた",
                    "desc":"",
                    "path":NSBundle.mainBundle().pathForResource("test_sandart", ofType: "mov")!,
                    "duration":19,
                    "caption_start":0,
                    "caption_end":5,
                ],
                ["title":"シューート！",
                    "desc":"",
                    "path":NSBundle.mainBundle().pathForResource("test_soccer", ofType: "mov")!,
                    "duration":5,
                    "caption_start":3,
                    "caption_end":5,
                ],
                ["title":"わーお！ジャイアン…",
                    "desc":"",
                    "path":NSBundle.mainBundle().pathForResource("test_takeshi", ofType: "mov")!,
                    "duration":13,
                    "caption_start":3,
                    "caption_end":6,
                ],
                ["title":"えびぞりジャンプ",
                    "desc":"",
                    "path":NSBundle.mainBundle().pathForResource("test_ebizori", ofType: "mov")!,
                    "duration":4,
                    "caption_start":0,
                    "caption_end":2,
                ]
        ]
    ]
    
    //var mock_data = [pikashi_data, jumbo_data, shoshomi_data, hajipion_data]
    //[
        
//        ["name":"yamakai",
//            "image_path":NSBundle.mainBundle().pathForResource("yamakai", ofType: "jpg")!,
//            "timeline":
//                [
//                    ["title":"渋谷でねこが喧嘩してた…！！",
//                        "desc":"",
//                        "path":NSBundle.mainBundle().pathForResource("test_catfight", ofType: "mov")!,
//                        "duration":11,
//                        "caption_start":5,
//                        "caption_end":9,
//                    ],
//                    ["title":"ブーケトス、お父さんがとっちゃった！",
//                        "desc":"",
//                        "path":NSBundle.mainBundle().pathForResource("test_bouquet", ofType: "mov")!,
//                        "duration":10,
//                        "caption_start":6,
//                        "caption_end":9,
//                    ]
//            ]
//        ]
    //]
    
    class var sharedInstance : MockDataManaber {
        struct Static {
            static var onceToken : dispatch_once_t = 0
            static var instance : MockDataManaber? = nil
        }
        dispatch_once(&Static.onceToken) {
            Static.instance = MockDataManaber()
        }
        return Static.instance!
    }

    func setup(){
        print("[Create mock data]")
        //self.deleteAll()
        self.createFromMock()
        self.showMovieData()
        self.setDefaultUser()
    }
    
    // MARK: check datas
    func showMovieData(){
        print("-------------USER--------------")
        let users = User.MR_findAll() as! [User]
        for user:User in users{
            NSLog("User - %@", user.name)
            for timeline in user.timelines{
                let t = timeline as! Timeline
                print(t.title)
            }
        }
        print("-------------TIMELINE--------------")
        let timelines = Timeline.MR_findAll() as! [Timeline]
        for timeline in timelines {
            print(timeline.title)
            let user = timeline.user
            print(user.name)
        }
        print("-------------MOVIE--------------")
        let movies = Movie.MR_findAll() as! [Movie]
        for movie in movies {
            print(movie.path)
        }
    }
    
    // MARK: Setup initial data
    
    func setDefaultUser(){
        let user = User.MR_findFirst()
        UserManager.sharedInstance.loginUser = user
    }
    
    func deleteAll(){
        User.MR_truncateAll()
        Timeline.MR_truncateAll()
        Movie.MR_truncateAll()
    }

    func createFromMock(){
        let mock_data = [self.pikashi_data, self.jumbo_data, self.shoshomi_data, self.hajipion_data]
        for user:NSDictionary in mock_data{
            var u = self.createUserSmaple(user["name"] as! String, image_path:user["image_path"] as! String)
            for timeline in user["timeline"] as! NSArray{
                self.createTimelineToUser(u, title: timeline["title"] as! String, desc: "",
                    path: timeline["path"] as! String,
                    caption_start: timeline["caption_start"] as! Float, caption_end: timeline["caption_end"] as! Float, duration:timeline["duration"] as! Float)
            }
        }
    }
    
    func createUserSmaple(name:String, image_path:String)->User{
        var user = User.MR_createEntity()!
        user.name = name
        user.image_path = image_path.stringByReplacingOccurrencesOfString(NSBundle.mainBundle().resourcePath!, withString: "", options: [], range: nil)
        user.managedObjectContext?.MR_saveToPersistentStoreAndWait()
        return user
    }
    
    func createTimelineToUser(user:User, title:String, desc:String, path:String, caption_start:Float!,caption_end:Float!, duration:Float!){
        var timeline = Timeline.MR_createEntity()!
        timeline.title = title
        timeline.desc = desc
        timeline.movie = self.createMovieToTimeLine(timeline, path: path, caption_start: caption_start, caption_end: caption_end, duration: duration)
        timeline.user = user
        user.addTimelinesObject(timeline)
        //timeline.managedObjectContext?.MR_saveToPersistentStoreAndWait()
        user.managedObjectContext?.MR_saveToPersistentStoreAndWait()
    }
    
    func createMovieToTimeLine(timeline:Timeline, path:String, caption_start:Float!,caption_end:Float!, duration:Float!)->Movie{
        var movie = Movie.MR_createEntity()!
        movie.path = path.stringByReplacingOccurrencesOfString(NSBundle.mainBundle().resourcePath!, withString: "", options: [], range: nil)
        movie.caption_end_sec = caption_end
        movie.caption_start_sec = caption_start
        movie.update_at = NSDate()
        movie.duration = duration
        timeline.movie = movie
        return movie
    }
}



/*
let data1 = Timeline.MR_createEntity() as Timeline
data1.title = "pika_shi"
data1.desc = "華麗なるウェディングキス"
data1.path = NSBundle.mainBundle().pathForResource("test_kiss", ofType: "mov")!
data1.created_at = NSDate()
data1.path = data1.path.stringByReplacingOccurrencesOfString(NSBundle.mainBundle().resourcePath!, withString: "", options: nil, range: nil)
data1.user_image_path = NSBundle.mainBundle().pathForResource("pika_shi", ofType: "jpg")!
data1.user_image_path = data1.user_image_path.stringByReplacingOccurrencesOfString(NSBundle.mainBundle().resourcePath!, withString: "", options: nil, range: nil)
data1.managedObjectContext?.MR_saveToPersistentStoreAndWait()

let data2 = Timeline.MR_createEntity() as Timeline
data2.title = "jumbOS5"
data2.desc = "イヌローさん胴上げわず"
data2.path = NSBundle.mainBundle().pathForResource("test_inuro", ofType: "mov")!
data2.path = data2.path.stringByReplacingOccurrencesOfString(NSBundle.mainBundle().resourcePath!, withString: "", options: nil, range: nil)
data2.user_image_path = NSBundle.mainBundle().pathForResource("jumbOS5", ofType: "jpg")!
data2.user_image_path = data2.user_image_path.stringByReplacingOccurrencesOfString(NSBundle.mainBundle().resourcePath!, withString: "", options: nil, range: nil)
data2.created_at = NSDate()
data2.managedObjectContext?.MR_saveToPersistentStoreAndWait()

let data3 = Timeline.MR_createEntity() as Timeline
data3.title = "shosomi"
data3.desc = "角度詐欺やってみた(^O^)／"
data3.path = NSBundle.mainBundle().pathForResource("test_fraud", ofType: "mov")!
data3.path = data3.path.stringByReplacingOccurrencesOfString(NSBundle.mainBundle().resourcePath!, withString: "", options: nil, range: nil)
data3.user_image_path = NSBundle.mainBundle().pathForResource("shosomi", ofType: "jpg")!
data3.user_image_path = data3.user_image_path.stringByReplacingOccurrencesOfString(NSBundle.mainBundle().resourcePath!, withString: "", options: nil, range: nil)
data3.created_at = NSDate()
data3.managedObjectContext?.MR_saveToPersistentStoreAndWait()

let data4 = Timeline.MR_createEntity() as Timeline
data4.title = "hajipion"
data4.desc = "隅田川の花火大会なう♡"
data4.path = NSBundle.mainBundle().pathForResource("test_fireworks", ofType: "mov")!
data4.path = data4.path.stringByReplacingOccurrencesOfString(NSBundle.mainBundle().resourcePath!, withString: "", options: nil, range: nil)
data4.user_image_path = NSBundle.mainBundle().pathForResource("hajipion", ofType: "jpg")!
data4.user_image_path = data4.user_image_path.stringByReplacingOccurrencesOfString(NSBundle.mainBundle().resourcePath!, withString: "", options: nil, range: nil)
data4.created_at = NSDate()
data4.managedObjectContext?.MR_saveToPersistentStoreAndWait()

let data5 = Timeline.MR_createEntity() as Timeline
data5.title = "yamakai"
data5.desc = "渋谷でねこが喧嘩してた…！！"
data5.path = NSBundle.mainBundle().pathForResource("test_catfight", ofType: "mov")!
data5.created_at = NSDate()
data5.path = data5.path.stringByReplacingOccurrencesOfString(NSBundle.mainBundle().resourcePath!, withString: "", options: nil, range: nil)
data5.user_image_path = NSBundle.mainBundle().pathForResource("yamakai", ofType: "jpg")!
data5.user_image_path = data5.user_image_path.stringByReplacingOccurrencesOfString(NSBundle.mainBundle().resourcePath!, withString: "", options: nil, range: nil)
data5.managedObjectContext?.MR_saveToPersistentStoreAndWait()

let data6 = Timeline.MR_createEntity() as Timeline
data6.title = "pika_shi"
data6.desc = "ブーケトス、お父さんがとっちゃった！"
data6.path = NSBundle.mainBundle().pathForResource("test_bouquet", ofType: "mov")!
data6.path = data6.path.stringByReplacingOccurrencesOfString(NSBundle.mainBundle().resourcePath!, withString: "", options: nil, range: nil)
data6.user_image_path = NSBundle.mainBundle().pathForResource("pika_shi", ofType: "jpg")!
data6.user_image_path = data6.user_image_path.stringByReplacingOccurrencesOfString(NSBundle.mainBundle().resourcePath!, withString: "", options: nil, range: nil)
data6.created_at = NSDate()
data6.managedObjectContext?.MR_saveToPersistentStoreAndWait()

let data7 = Timeline.MR_createEntity() as Timeline
data7.title = "jumbOS5"
data7.desc = "泣いちゃった。ごめんｗ"
data7.path = NSBundle.mainBundle().pathForResource("test_baby", ofType: "mov")!
data7.path = data7.path.stringByReplacingOccurrencesOfString(NSBundle.mainBundle().resourcePath!, withString: "", options: nil, range: nil)
data7.user_image_path = NSBundle.mainBundle().pathForResource("jumbOS5", ofType: "jpg")!
data7.user_image_path = data7.user_image_path.stringByReplacingOccurrencesOfString(NSBundle.mainBundle().resourcePath!, withString: "", options: nil, range: nil)
data7.created_at = NSDate()
data7.managedObjectContext?.MR_saveToPersistentStoreAndWait()

let data8 = Timeline.MR_createEntity() as Timeline
data8.title = "shosomi"
data8.desc = "舞う風とともに去りぬ"
data8.path = NSBundle.mainBundle().pathForResource("test_leaf", ofType: "mov")!
data8.path = data8.path.stringByReplacingOccurrencesOfString(NSBundle.mainBundle().resourcePath!, withString: "", options: nil, range: nil)
data8.user_image_path = NSBundle.mainBundle().pathForResource("shosomi", ofType: "jpg")!
data8.user_image_path = data8.user_image_path.stringByReplacingOccurrencesOfString(NSBundle.mainBundle().resourcePath!, withString: "", options: nil, range: nil)
data8.created_at = NSDate()
data8.managedObjectContext?.MR_saveToPersistentStoreAndWait()

let data9 = Timeline.MR_createEntity() as Timeline
data9.title = "hajipion"
data9.desc = "森と少女"
data9.path = NSBundle.mainBundle().pathForResource("test_mori", ofType: "mov")!
data9.path = data9.path.stringByReplacingOccurrencesOfString(NSBundle.mainBundle().resourcePath!, withString: "", options: nil, range: nil)
data9.user_image_path = NSBundle.mainBundle().pathForResource("hajipion", ofType: "jpg")!
data9.user_image_path = data9.user_image_path.stringByReplacingOccurrencesOfString(NSBundle.mainBundle().resourcePath!, withString: "", options: nil, range: nil)
data9.created_at = NSDate()
data9.managedObjectContext?.MR_saveToPersistentStoreAndWait()

let data10 = Timeline.MR_createEntity() as Timeline
data10.title = "yamakai"
data10.desc = "ツールド東北2014！！つかれた〜"
data10.path = NSBundle.mainBundle().pathForResource("test_tourdetohoku", ofType: "mov")!
data10.path = data10.path.stringByReplacingOccurrencesOfString(NSBundle.mainBundle().resourcePath!, withString: "", options: nil, range: nil)
data10.user_image_path = NSBundle.mainBundle().pathForResource("yamakai", ofType: "jpg")!
data10.user_image_path = data10.user_image_path.stringByReplacingOccurrencesOfString(NSBundle.mainBundle().resourcePath!, withString: "", options: nil, range: nil)
data10.created_at = NSDate()
data10.managedObjectContext?.MR_saveToPersistentStoreAndWait()

*/